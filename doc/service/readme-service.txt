--------------------------------------------------------------------------------------------------------------------------------
AZ ALKALMAZÁS TELEPÍTÉSE LINUX OPERÁCIÓS RENDSZERRE, SYSTEMD SERVICE-KÉNT:

- A "spring-start.jar" futtatásához legalább 11-es verziójú java futtatókörnyezet szükséges. Ennek ellenőrzése:

    /usr/lib/jvm/java-11-openjdk/bin/java -version

- A "spring-start.service" fájl tartalmaz egy bejegyzést, mely beállítja hogy a service milyen user a nevében fusson.
  Jelenleg ez van beállítva: "User=tomcat8"
  Amennyiben "tomcat8" user nincs a futtató gépen, akkor létre kell hozni, 
  vagy ezt a bejegyzést beállítani egy már meglévő user-re.

- A "spring-start.service" fájlt a futtató gépre kell másolni a következő könyvtárba:

    /etc/systemd/system

  A tulajdonosa, és a csoportja is "root" legyen.

- Létre kell hozni a futtató gépen a következő könyvtárakat:

    /opt/xesj-app/spring-start
    /opt/xesj-app/spring-start/config
    /opt/xesj-app/spring-start/log    (a könyvtárra való jogosultságot állítsuk át: "chmod 777 log")

- A "spring-start.jar" fájlt ebbe a könyvtárba kell másolni:

    /opt/xesj-app/spring-start

- Root-ként el kell indítani a systemd service-t:

    systemctl start spring-start

- A systemd service beállítása úgy, hogy egy rendszer boot után is elinduljon:

    systemctl enable spring-start

- Az alkalmazás működésének ellenőrzése, a következő url meghívása web böngészőből:

    http://KULSO-HOST:47999/spring-start

  ahol a "KULSO-HOST" átírandó a futtató gép IP címére, vagy host nevére.

--------------------------------------------------------------------------------------------------------------------------------
SYSTEMD SERVICE PARANCSOK AZ ALKALMAZÁS INDÍTÁSÁHOZ, LEÁLLÍTÁSÁHOZ:

Indítás:

  systemctl start spring-start

Újraindítás:

  systemctl restart spring-start

Leállítás:

  systemctl stop spring-start

Státusz megtekintés:
 
  systemctl status -l spring-start
--------------------------------------------------------------------------------------------------------------------------------