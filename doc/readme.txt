+--------+
| README |
--------------------------------------------------------------------------------------------------------------------------------
Ez a projekt kiindulópont egy komolyabb spring boot alkalmazás kialakításához. 
A következőket technológiákat tartalmazza: 

  - Spring boot
  - Thymeleaf
  - JdbcTemplate (H2 memória adatbázis használatával)
  - Bootstrap
  - Xesjslib
--------------------------------------------------------------------------------------------------------------------------------