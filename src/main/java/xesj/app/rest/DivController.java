package xesj.app.rest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xesj.app.util.json_response.JsonResponse;
import xesj.app.util.json_response.JsonResponseError;

/**
 * Számok osztása rest service
 */
@RestController
public class DivController {
  
  public static final String PATH = "/rest/div";
  
  /**
   * GET: /rest/div
   */
  @GetMapping(PATH)
  public Object get(@RequestParam int a, @RequestParam int b) {

    if (b == 0) {
      JsonResponse jr = new JsonResponse()
        .setErrorType(JsonResponseError.Type.VALIDATION)
        .addErrorMessage("Az osztó nem lehet nulla!");
      return new ResponseEntity(jr, HttpStatus.BAD_REQUEST);
    }
    else {
      return new JsonResponse()
        .putData("a", a)
        .putData("b", b)
        .putData("a/b", 1.0 * a / b);
    }  

  }
  
  // ====
}
