package xesj.app.personal;
import java.time.LocalDate;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

/**
 * Személyes adatok form
 */
@Getter
@Setter
public class PersonalForm {
  
  /**
   * Név
   */
  private String nev;
  
  /**
   * Nem
   */
  private String nem;

  /**
   * Születési dátum
   */
  private LocalDate szuletesiDatum;

  /**
   * Születési hely
   */
  private String szuletesiHely;

  /**
   * Email cím
   */
  private String email;

  /**
   * Legmagasabb iskolai végzettség
   */
  private String vegzettseg;

  /**
   * Okmányok
   */  
  private List<Integer> okmanyok;

  /**
   * Fénykép
   */  
  private MultipartFile fenykep;

  /**
   * Élettörténet
   */
  private String elettortenet;
  
  /**
   * Hozzájárulás
   */
  private boolean hozzajarulas;
  
}
