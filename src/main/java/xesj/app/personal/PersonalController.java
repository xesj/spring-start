package xesj.app.personal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import xesj.app.system.MainSession;
import xesj.app.util.json_response.JsonResponse;
import xesj.app.util.main.Constant;
import xesj.app.util.main.Home;
import xesj.app.util.main.MainUtil;
import xesj.app.util.main.Page;
import xesj.spring.validation.validate.EmailValidate;
import xesj.spring.validation.validate.LengthValidate;
import xesj.spring.validation.validate.LocalDateRangeValidate;
import xesj.spring.validation.validate.RequiredValidate;
import xesj.tool.StringTool;

/**
 * Személyes adatok űrlap controller
 */
@Controller
public class PersonalController {
  
  public static final String 
    PATH_FORM = "/personal/form",
    PATH_SUMMARY = "/personal/summary",
    PATH_SETTLEMENT = "/personal/settlement";
  
  private static final String
    VIEW_FORM = "/personal/personal-form.html",
    VIEW_SUMMARY = "/personal/personal-summary.html";
  
  @Autowired MainSession session;
  @Autowired PersonalDictionary dictionary;
  @Autowired MainUtil mainUtil;

  /**
   * GET: /personal/form
   */  
  @GetMapping(PATH_FORM)
  public String getForm(Model model) {
    
    // Form lekérdezése
    var form = session.getPersonalForm();
    if (form == null) {
      form = new PersonalForm();
    }
    
    // Model beállítása
    model.addAttribute("page", new Page("Személyes adatok", Home.MENU));
    model.addAttribute("form", form);
    
    // View megjelenítése
    return VIEW_FORM;
    
  }

  /**
   * GET: /personal/summary
   */  
  @GetMapping(PATH_SUMMARY)
  public String getSummary(Model model) {
    
    // Form lekérdezése
    var form = session.getPersonalForm();
    if (form == null) {
      return Constant.REDIRECT + PATH_FORM; // Ha még nincsenek adatok, akkor az űrlapot kell kitölteni 
    }
    
    // Model összeállítása
    model.addAttribute("page", new Page("Személyes adatok összefoglaló", Home.MENU));
    model.addAttribute("form", form);
    model.addAttribute("nem", dictionary.getNemek().get(form.getNem()));
    model.addAttribute("vegzettseg", dictionary.getVegzettsegek().get(form.getVegzettseg()));
    List<String> okmanyok = new ArrayList<>(); 
    for (Integer i: form.getOkmanyok()) {
      okmanyok.add(dictionary.getOkmanyok().get(i));
    }
    model.addAttribute("okmanyok", okmanyok);
    if (form.getFenykep() != null && !form.getFenykep().getOriginalFilename().isBlank()) {
      model.addAttribute("fenykep", form.getFenykep().getOriginalFilename());
    } 

    // View megjelenítése
    return VIEW_SUMMARY;
    
  }
  
  /**
   * POST: /personal/form
   */  
  @PostMapping(PATH_FORM)
  public String post(@ModelAttribute("form") PersonalForm form, BindingResult result, Model model) {
    
    // Validáció
    validate(form, result);
    
    // Validáció eredményének értékelése
    if (result.hasErrors()) {
      model.addAttribute("page", new Page("Személyes adatok", Home.MENU));
      return VIEW_FORM;
    }
    else {
      session.setPersonalForm(form);
      return Constant.REDIRECT + PATH_SUMMARY;
    }
    
  }
  
  /**
   * Form validáció
   */
  private void validate(PersonalForm form, BindingResult result) {

    // Előkészítés
    var context = mainUtil.formValidationContext(result);
    String field;
    
    // Név
    field = "nev";
    context.add(field, 
      () -> new RequiredValidate(form.getNev()),
      () -> new LengthValidate(form.getNev(), 1L, 32L)
    );
    
    // Nem
    field = "nem";
    context.add(field, () -> new RequiredValidate(form.getNem()));
    if (!dictionary.getNemek().keySet().contains(form.getNem())) {
      context.add(field, "Ilyen adat nem létezik!");
    }
    
    // Születési dátum
    field = "szuletesiDatum";
    LocalDate minimum = LocalDate.now().minusYears(150);
    LocalDate maximum = LocalDate.now();
    context.add(field, 
      () -> new RequiredValidate(form.getSzuletesiDatum()),
      () -> new LocalDateRangeValidate(form.getSzuletesiDatum(), minimum, maximum, "uuuu.MM.dd")
    );
    
    // Email
    field = "email";
    context.add(field, () -> new EmailValidate(form.getEmail()));
    
    // Végzettség
    field = "vegzettseg";
    context.add(field, () -> new RequiredValidate(form.getVegzettseg()));
    if (!dictionary.getVegzettsegek().keySet().contains(form.getVegzettseg())) {
      context.add(field, "Ilyen adat nem létezik!");
    }
    
    // Okmányok
    field = "okmanyok";
    for (Integer okmany: form.getOkmanyok()) {
      if (!dictionary.getOkmanyok().keySet().contains(okmany)) {
        context.add(field, "Ilyen adat nem létezik!");
      }
    }
    
    // Fénykép
    field = "fenykep";
    var fenykep = form.getFenykep();
    if (fenykep != null && fenykep.getSize() > 0) {
      if (fenykep.getSize() > 256 * 1024) {
        context.add(field, "A fénykép mérete maximum 256 kilobájt lehet!");
      }
      String extension = "";
      int pos = fenykep.getOriginalFilename().lastIndexOf('.');
      if (pos > -1) {
        extension = fenykep.getOriginalFilename().substring(pos + 1).toLowerCase();
      }
      if (!StringTool.in(extension, "jpg", "jpeg", "png")) {
        context.add(field, "A fénykép fájl kiterjesztése csak a következő lehet: jpg, jpeg, png");
      }
    }  
    
    // Élettörténet
    field = "elettortenet";
    context.add(field, 
      () -> new LengthValidate(form.getElettortenet(), 1L, 4096L)
    );
    
  }

  /**
   * GET: /personal/settlement
   * HSDE választ küld, a json válasz "data" struktúrája:
   *
   *  data: {
   *    text: [string, string, ...],
   *    total: boolean
   *  }
   */  
  @GetMapping(PATH_SETTLEMENT)
  @ResponseBody
  public JsonResponse settlement(@RequestParam(required = false, defaultValue = "?") String value) {
    
    // Előkészítés
    final int MAX_RESULT = 8;
    boolean total = true;
    value = value.trim().toLowerCase();
    
    // Települések összegyűjtése
    List<String> telepulesek = new ArrayList<>();
    for (String telepules: dictionary.getTelepulesek()) {
      if (telepules.toLowerCase().startsWith(value)) {
        if (telepulesek.size() < MAX_RESULT) {
          telepulesek.add(telepules);
        }
        else {
          total = false;
          break;
        }
      }
    }
    
    // Válasz
    return new JsonResponse()
      .putData("text", telepulesek)
      .putData("total", total);
    
  }
  
  // ====
}
