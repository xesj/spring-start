package xesj.app.user;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import xesj.app.util.main.Home;
import xesj.app.util.main.Page;

/**
 * Felhasználó kijelentkezés controller
 */
@Controller
public class LogoutController {

  public static final String
    PATH = "/user/logout",
    VIEW = "/user/logout.html";
  
  /**
   * GET: /user/logout
   */
  @GetMapping(PATH)
  public String get(Model model, HttpSession httpSession) {
    
    model.addAttribute("page", new Page("Kijelentkezés", null));
    httpSession.invalidate();
    return VIEW;
    
  }
  
  // ====
}
