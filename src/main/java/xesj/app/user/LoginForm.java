package xesj.app.user;
import lombok.Getter;
import lombok.Setter;

/**
 * Bejelentkezés form
 */
@Getter
@Setter
public class LoginForm {
  
  private String username;
  private String password;
  
}
