package xesj.app.user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import xesj.app.system.MainSession;
import xesj.app.menu.MenuController;
import xesj.app.util.main.Constant;
import xesj.app.dao.LoginUserDTO;
import xesj.app.dao.LoginUserDAO;
import xesj.app.util.main.MainUtil;
import xesj.app.util.main.Page;
import xesj.spring.validation.FormValidationContext;
import xesj.spring.validation.validate.RequiredValidate;

/**
 * Felhasználó bejelentkezés controller
 */
@Controller
public class LoginController {

  public static final String
    PATH = "/user/login",
    VIEW = "/user/login.html",
    PAGE_TITLE = "Bejelentkezés";
  
  @Autowired MainSession session;
  @Autowired LoginUserDAO loginUserDAO;
  @Autowired MainUtil mainUtil;
  
  /**
   * GET: /user/login
   */
  @GetMapping(PATH)
  public String get(Model model) {
    
    LoginForm form = new LoginForm();
    model.addAttribute("form", form);
    model.addAttribute("page", new Page(PAGE_TITLE, null));
    return VIEW;

  }

  /**
   * POST: /user/login
   */
  @PostMapping(PATH)
  public String post(@ModelAttribute("form") LoginForm form, BindingResult result, Model model) {

    // Validáció
    LoginUserDTO loginUserDTO = validate(form, result);
    if (result.hasErrors()) {
      model.addAttribute("page", new Page(PAGE_TITLE, null));
      return VIEW;
    }
    else {
      // Sikeres bejelentkezés
      session.setLoginUserDTO(loginUserDTO);
      return Constant.REDIRECT + MenuController.PATH;
    }
    
  }
  
  /**
   * Validáció
   */
  private LoginUserDTO validate(LoginForm form, BindingResult result) {
    
    // Előkészítés
    FormValidationContext context = mainUtil.formValidationContext(result);
    LoginUserDTO loginUserDTO = null;
    
    // Felhasználónév ellenőrzése
    context.add("username", () -> new RequiredValidate(form.getUsername()));

    // Jelszó ellenőrzése
    context.add("password", () -> new RequiredValidate(form.getPassword(), false));
    
    // A felhasználónév/jelszó ellenőrzése adatbázisból
    if (!result.hasErrors()) {
      loginUserDTO = loginUserDAO.lekerdezesFelhasznalonevJelszoAlapjan(form.getUsername(), form.getPassword());
      if (loginUserDTO == null) {
        context.add(null, "A felhasználónév - jelszó páros érvénytelen!");
      }  
    }

    // Válasz
    return loginUserDTO;
    
  }
  
  // ====
}
