package xesj.app.system;
import java.util.Date;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import xesj.app.util.main.Home;
import xesj.app.util.main.Page;
import xesj.tool.DateTool;

/**
 * 3-as szintű error handler 
 */
@Controller
public class MainErrorController implements ErrorController {

  public static final String PATH = "/error";
  private static final String VIEW = "/unexpected-error.html";

  @Autowired ErrorAttributes errorAttributes;
  
  /**
   * Error handler
   */
  @RequestMapping(PATH)
  public String errorHandler(Model model, WebRequest webRequest) {

    // Hiba objektumok lekérdezése
    ErrorAttributeOptions errorAttributeOptions = ErrorAttributeOptions.of(
      ErrorAttributeOptions.Include.MESSAGE, ErrorAttributeOptions.Include.STACK_TRACE
    );
    Map<String, Object> attributeMap = errorAttributes.getErrorAttributes(webRequest, errorAttributeOptions);
    
    // Model összeállítása
    model.addAttribute("page", new Page("Váratlan hiba", Home.MENU));
    model.addAttribute("timestamp", DateTool.formatSec((Date)attributeMap.get("timestamp")));
    model.addAttribute("status", attributeMap.get("status"));
    model.addAttribute("error", attributeMap.get("error"));
    model.addAttribute("message", attributeMap.get("message"));
    model.addAttribute("path", attributeMap.get("path"));
    model.addAttribute("trace", attributeMap.get("trace"));
    
    // View megjelenítése
    return VIEW;
    
  }
  
  // ====
}
