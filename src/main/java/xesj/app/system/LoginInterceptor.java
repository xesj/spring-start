package xesj.app.system;
import java.io.IOException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;
import xesj.app.dao.LoginUserDTO;
import xesj.app.user.LoginController;

/**
 * Az osztály ellenőrzi hogy a felhasználó be van-e jelentkezve. Ha nincs akkor a bejelentkező lapra redirect-ál.
 */
@Service
public class LoginInterceptor implements HandlerInterceptor {
  
  @Autowired MainSession session;
  @Autowired Property property;

  /**
   * Műveletek a controllerek lefutása előtt
   */
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws IOException {
    
    // Automatikus login ?
    if (property.getUsers().containsKey(property.getAutoLogin())) {
      var dto = new LoginUserDTO();
      dto.setId(1);
      dto.setUsername(property.getAutoLogin());
      session.setLoginUserDTO(dto);
    }
    
    // Ellenőrzés
    if (!session.isLogged()) {
      // A felhasználó nincs bejelentkezve, átirányítás a login lapra.
      response.sendRedirect(request.getContextPath() + LoginController.PATH);
      return false;
    }
    else {
      return true;
    }

  }
  
  // ====
}
