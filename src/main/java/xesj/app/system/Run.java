package xesj.app.system;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Program futtatás
 */
@SpringBootApplication(scanBasePackages = "xesj.app")
public class Run {
  
  /**
   * Spring Boot inicializálás
   */
  public static void main(String[] args) {

    SpringApplication.run(Run.class, args);

  }
  
  // ====
}
