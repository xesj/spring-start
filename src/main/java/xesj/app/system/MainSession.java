package xesj.app.system;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import xesj.app.dao.LoginUserDTO;
import xesj.app.personal.PersonalForm;

/**
 * Fő session objektum
 */
@Service
@SessionScope
@Getter
@Setter
public class MainSession {
  
  /**
   * A bejelentkezett felhasználó
   */
  private LoginUserDTO loginUserDTO;

  /**
   * Személyes adatok
   */
  private PersonalForm personalForm;
  
  /**
   * A felhasználó be van jelentkezve ?
   */
  public boolean isLogged() {
    
    return (loginUserDTO != null);

  }
  
  // ====
}
