package xesj.app.system;
import java.util.LinkedHashMap;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import xesj.long_process.LongProcess;

/**
 * Fő application objektum
 */
@Service
@Getter
public class MainApplication {

  /**
   * Application context nem spring bean-ek számára
   */
  @Getter
  @Setter
  private static ApplicationContext context;
  
  /**
   * Folyamat azonosító
   */
  private long processId = 0;
  
  /**
   * Folyamatok
   */
  private LinkedHashMap<Long, LongProcess> processes = new LinkedHashMap<>();
  
  /**
   * Új folyamat hozzáadása
   */
  public void addProcess(LongProcess longProcess) {
    
    processId++;
    processes.put(processId, longProcess);
    
  }

  // ====
}
