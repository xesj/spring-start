package xesj.app.system;
import java.io.IOException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * Az osztály ellenőrzi, hogy a request mérete a megengedett maximumon belül van-e. 
 * Ha nincs akkor hibát dob.
 */
@Service
public class RequestSizeInterceptor implements HandlerInterceptor {
  
  @Autowired Property property;

  /**
   * Műveletek a controllerek lefutása előtt
   */
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws IOException {
    
    // Ellenőrzés
    long contentLength = request.getContentLengthLong();
    if (contentLength > property.getMaxRequestSize()) {
      throw new RuntimeException("A http-request túl nagy méretű: " + contentLength + " bájt!");
    }
    
    // Rendben
    return true;

  }
  
  // ====
}
