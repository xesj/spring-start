package xesj.app.system;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import xesj.app.dao.InitDAO;
import xesj.app.long_process.process.MailLongProcess;
import xesj.app.long_process.process.PrimeLongProcess;

/**
 * Fő event listener
 */
@Service
public class MainEventListener {
  
  @Autowired ApplicationContext applicationContext;
  @Autowired MainApplication application;
  @Autowired InitDAO initDAO;
 
  @EventListener
  public void onApplicationEvent(ContextRefreshedEvent event) {
    
    // Application context beállítása statikus változóba
    MainApplication.setContext(applicationContext);

    // Adatbázis struktúra létrehozása
    initDAO.strukturaLetrehozas();
    
    // Hosszan tartó folyamatok létrehozása
    createLongProcesses();
    
  }
  
  /**
   * Hosszan tartó folyamatok létrehozása 
   */
  private void createLongProcesses() {

    // Nincs jog egy email küldő folyamatra (1)
    application.addProcess(new MailLongProcess(101, 50));

    // Csak olvasási jog van egy email küldő folyamatra (2)
    application.addProcess(new MailLongProcess(102, 60));

    // Email küldő, mely SUCCESS státusszal ér véget (3)
    application.addProcess(new MailLongProcess(700, 50));

    // Email küldő, mely ERROR státusszal ér véget (4)
    application.addProcess(new MailLongProcess(103, 40));

    // Prímszám kereső, várakozás nélküli folyamat (5)
    application.addProcess(new PrimeLongProcess(2, 1_000_000, 65536));

    // Prímszám kereső, óriás számok keresése (6)
    application.addProcess(new PrimeLongProcess(Long.MAX_VALUE, 2, 32));

    // Prímszám kereső, óriás számok keresése (7)
    application.addProcess(new PrimeLongProcess(Long.MAX_VALUE, 99999999, 64));
    
  }
  
  // ====
}
