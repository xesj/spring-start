package xesj.app.system;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;
import org.springframework.web.servlet.mvc.WebContentInterceptor;
import xesj.app.user.LoginController;
import xesj.app.user.LogoutController;
import xesj.spring.property_editor.DatePropertyEditor;
import xesj.spring.property_editor.LocalDatePropertyEditor;
import xesj.spring.property_editor.StringPropertyEditor;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.ValidationContext;
import xesj.tool.LocaleTool;

/**
 * Fő konfiguráció
 */
@Configuration
@ControllerAdvice
public class MainConfiguration implements WebMvcConfigurer {
  
  @Autowired RequestSizeInterceptor requestSizeInterceptor;
  @Autowired LoginInterceptor loginInterceptor;
  
  /**
   * Interceptor regisztrációk
   */
  @Override
  public void addInterceptors(InterceptorRegistry registry) {

    // Request size interceptor regisztráció
    registry.addInterceptor(requestSizeInterceptor).order(1);
    
    // WebContent interceptor regisztráció. Cache letiltva a weblapok számára.
    WebContentInterceptor wci = new WebContentInterceptor();
    wci.addCacheMapping(CacheControl.noStore(), "/**");
    registry.addInterceptor(wci).excludePathPatterns("/static/**").order(2);

    // Login interceptor regisztráció
    registry.addInterceptor(loginInterceptor).excludePathPatterns(
      "/error",  
      "/static/**", 
      LoginController.PATH,  
      LogoutController.PATH
    ).order(3);
    
  }
  
  /**
   * Globális initbinder
   */
  @InitBinder
  public void initBinder(WebDataBinder binder) throws Exception {
    
    binder.registerCustomEditor(String.class, new StringPropertyEditor());
    binder.registerCustomEditor(Date.class, new DatePropertyEditor("yyyy.MM.dd"));
    binder.registerCustomEditor(LocalDate.class, new LocalDatePropertyEditor("uuuu.MM.dd"));

  }
  
  /**
   * Locale beállítása fixen magyarra
   */
  @Bean
  public LocaleResolver localeResolver() {
    
    Locale.setDefault(LocaleTool.LOCALE_HU);
    return new FixedLocaleResolver(Locale.getDefault());

  }
  
  /**
   * Datasource a H2 memory adatbázishoz
   */
  @Bean
  @ConfigurationProperties("app.db")
  public DataSource dataSource() {

    return DataSourceBuilder.create().build();

  }
  
  /**
   * NamedParameterJdbcTemplate a H2 memory adatbázishoz
   */
  @Bean
  public NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Autowired DataSource datasource) {

    return new NamedParameterJdbcTemplate(datasource);

  }
  
  /**
   * ValidationContext bean. Nem dob exception-t.
   */
  @Bean("validationContext")
  @RequestScope
  public ValidationContext validationContext(@Autowired MessageSource messageSource) {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    return new ValidationContext(msl, 1, null, null);

  }

  /**
   * ValidationContext bean. Az első hibánál exception-t dob.
   */
  @Bean("validationContextThrow1")
  @RequestScope
  public ValidationContext validationContextThrow1(@Autowired MessageSource messageSource) {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    return new ValidationContext(msl, 1, null, 1);

  }
  
  /**
   * Cache engedélyezve a static resource-ok számára. A public cache-ben 17 percig tárolhatók.
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {

    registry
      .addResourceHandler("/**")
      .addResourceLocations("classpath:/public/")
      .setCacheControl(CacheControl.maxAge(1024, TimeUnit.SECONDS).mustRevalidate().cachePublic());

  }

  // ====
}
