package xesj.app.system;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import xesj.app.menu.MenuController;
import xesj.app.util.main.Constant;

/**
 * Fő controller
 */
@Controller
public class MainController {

  public static final String 
    PATH_ROOT = "/",
    PATH_DIVBYZERO = "/divbyzero",
    PATH_EXIT = "/exit-92837267652306180863952329447728";
  
  /**
   * GET: /
   */
  @GetMapping(PATH_ROOT)
  public String root() {

    return Constant.REDIRECT + MenuController.PATH;

  }

  /**
   * GET: /divbyzero
   */
  @GetMapping(PATH_DIVBYZERO)
  public int divbyzero() {

    return 1 / 0;

  }

  /**
   * GET: /exit-...
   */
  @GetMapping(PATH_EXIT)
  public void exit() {

    System.exit(0);

  }
  
  // ====
}
