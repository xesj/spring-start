package xesj.app.system;
import java.util.Map;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

/**
 * Alkalmazás jellemzők
 */
@Service
@ConfigurationProperties("app")
@Validated
@Getter
@Setter
public class Property {
  
  @NotNull
  private String title;

  @NotEmpty
  private Map<String, String> users;
  
  private String autoLogin;
  
  @NotNull
  private long maxRequestSize;

}
