package xesj.app.util.main;
import xesj.app.menu.MenuController;

/**
 * Navigáció az oldal felső csíkjának közepére kattintva
 */
public class Home {
  
  /**
   * Ablak bezárása
   */
  public static final String CLOSE = "(CLOSE)";
  
  /**
   * Menü
   */
  public static final String MENU = MenuController.PATH;

  // ====  
}
