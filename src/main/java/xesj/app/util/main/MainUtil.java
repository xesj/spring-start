package xesj.app.util.main;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import xesj.spring.validation.FormValidationContext;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

/**
 * Fő utility
 */
@Service
public class MainUtil {
  
  @Autowired MessageSource messageSource;
  
  /**
   * FormValidationContext lekérdezése.
   */
  public FormValidationContext formValidationContext(BindingResult bindingResult) {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    return new FormValidationContext(bindingResult, msl, 1, null);

  }

  // ====
}
