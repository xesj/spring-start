package xesj.app.util.main;
import xesj.app.system.MainApplication;
import xesj.app.system.Property;
import xesj.spring.validation.ValidationContext;

/**
 * Spring bean utility
 */
public class SpBean {
  
  /**
   * Spring bean lekérdezése típus alapján
   */
  public static <T> T get(Class<T> cls) {

    return MainApplication.getContext().getBean(cls);
    
  }
  
  /**
   * Property spring bean lekérdezése
   */
  public static Property property() {
    
    return MainApplication.getContext().getBean(Property.class);

  }

  /**
   * MainApplication spring bean lekérdezése
   */
  public static MainApplication application() {
    
    return MainApplication.getContext().getBean(MainApplication.class);

  }

  /**
   * "validationContext" nevű spring bean lekérdezése
   */
  public static ValidationContext validationContext() {

    return MainApplication.getContext().getBean("validationContext", ValidationContext.class);

  }

  /**
   * "validationContextThrow1" nevű spring bean lekérdezése
   */
  public static ValidationContext validationContextThrow1() {

    return MainApplication.getContext().getBean("validationContextThrow1", ValidationContext.class);

  }

  // ====
}
