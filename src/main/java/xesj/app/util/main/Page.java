package xesj.app.util.main;

/**
 * Általános oldal információk
 */
public class Page {
  
  public String title;
  public String home;
  
  /**
   * Konstruktor
   * @param title Az oldal címe
   * @param home 
   *        Navigáció az oldal felső csíkjának közepére kattintva. A Home osztály konstansai is használhatók.
   *        Speciális esetek:
   *        <ul>
   *          <li>null = Nincs navigáció</li>
   *          <li>"(CLOSE)" = Ablak bezárása</li> 
   *        </ul>
   */
  public Page(String title, String home) {

    this.title = title;
    this.home = home;

  }
  
  // ====
}
