package xesj.app.util.json_response;
import java.util.List;

/**
 * Általános json típusú válasz "error" része
 */
public class JsonResponseError {
  
  /** 
   * Hiba típusa 
   */
  public enum Type {
    /** 
     * Validációs hiba 
     */
    VALIDATION,
    /** 
     * Programhiba 
     */
    UNEXPECTED,
  }
  
  public Type type;
  public List<String> message;
  public String stack;
  
  // ====
}
