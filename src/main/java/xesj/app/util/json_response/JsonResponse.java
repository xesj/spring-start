package xesj.app.util.json_response;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Általános json típusú válasz
 */
public class JsonResponse {
  
  public Map data;
  public JsonResponseError error;
  
  /**
   * "data" tag inicializálása (LinkedHashMap), ha még nem történt meg.
   */
  private JsonResponse createData() {
    
    if (data == null) {
      data = new LinkedHashMap<String, Object>();
    }
    return this;
    
  }

  /**
   * "error" tag inicializálása, ha még nem történt meg.
   */
  private JsonResponse createError() {
    
    if (error == null) {
      error = new JsonResponseError();
      error.message = new ArrayList<>();
    }
    return this;
    
  }
  
  /**
   * Adat tárolása a "data" map-ben.
   * @param key Kulcs
   * @param value Érték
   */
  public JsonResponse putData(String key, Object value) {

    createData();
    data.put(key, value);
    return this;
    
  }

  /**
   * "error" tag "type" részének beállítása.
   */
  public JsonResponse setErrorType(JsonResponseError.Type type) {
    
    createError();
    error.type = type;
    return this;
    
  }

  /**
   * "error" tag "message" részéhez üzenet hozzáadás.
   * @param message Üzenet
   */  
  public JsonResponse addErrorMessage(String message) {

    createError();
    error.message.add(message);
    return this;

  }
  
  /**
   * "error" tag "message" részéhez üzenetek hozzáadása.
   * @param messages Üzenetek
   */  
  public JsonResponse addErrorMessages(List<String> messages) {

    createError();
    error.message.addAll(messages);
    return this;

  }

  /**
   * "error" tag "stack" részének beállítása.
   * @param stack Stack trace string formában.
   */
  public JsonResponse setErrorStack(String stack) {
    
    createError();
    error.stack = stack;
    return this;
    
  }
  
  // ====
}
