package xesj.app.menu;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import xesj.app.util.main.Page;
import xesj.tool.RandomTool;

/**
 * Menü controller
 */
@Controller
public class MenuController {
  
  public static final String PATH = "/menu";
  private static final String VIEW = "/menu.html";
  
  /**
   * GET: /menu
   */
  @GetMapping(PATH)
  public String get(Model model) {
    
    model.addAttribute("a", RandomTool.interval(0, 9));
    model.addAttribute("b", RandomTool.interval(0, 3));
    model.addAttribute("page", new Page("Menü", null));
    return VIEW;

  }
  
  // ====
}
