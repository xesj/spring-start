package xesj.app.dao;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import xesj.app.system.Property;

/**
 * H2 memória adatbázis inicializálása.
 */
@Repository
public class InitDAO {
  
  @Autowired NamedParameterJdbcTemplate jdbc;
  @Autowired Property property;
  
  /**
   * Adatbázis struktúra létrehozása, táblák feltöltése.
   * Az eljárás tetszőleges sokszor hívható, mivel nem próbálja meg duplán létrehozni és feltölteni az adatbázis objektumot. 
   */
  public void strukturaLetrehozas() {
    
    // LOGIN_USER tábla törlése, feltöltése
    jdbc.update(
      "DROP TABLE IF EXISTS login_user", 
      (Map)null
    );
    jdbc.update(
      "CREATE TABLE login_user(id bigint primary key, username varchar(100), password varchar(100))",
      (Map)null
    );
    int id = 1;
    for (String un: property.getUsers().keySet()) {
      jdbc.update(
        "INSERT INTO login_user(id, username, password) VALUES(:id, :username, :password)", 
        new MapSqlParameterSource()
          .addValue("id", id++)
          .addValue("username", un)
          .addValue("password", property.getUsers().get(un))
      );
    }

  }
  
  // ====
}
