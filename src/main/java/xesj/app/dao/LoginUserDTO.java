package xesj.app.dao;
import lombok.Getter;
import lombok.Setter;

/**
 * A bejelentkezett felhasználó 
 */
@Getter
@Setter
public class LoginUserDTO {
  
  private long id;
  private String username;
  
}
