package xesj.app.dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * LOGIN_USER dao 
 */
@Repository
public class LoginUserDAO {
 
  @Autowired NamedParameterJdbcTemplate jdbc;
  
  /**
   * Felhasználó keresése username/password alapján. 
   * @return A username/password-höz tartozó felhasználó. Ha ilyen nem létezik, akkor null-t ad vissza.
   */
  public LoginUserDTO lekerdezesFelhasznalonevJelszoAlapjan(String username, String password) {

    try {
      return jdbc.queryForObject(
        "SELECT * FROM login_user WHERE username = :username and password = :password", 
        new MapSqlParameterSource("username", username).addValue("password", password), 
        BeanPropertyRowMapper.newInstance(LoginUserDTO.class)
      );
    }
    catch (EmptyResultDataAccessException e) {
      return null;
    }

  }
  
  // ====
}
