package xesj.app.long_process.controller;
import jakarta.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xesj.long_process.LongProcess;
import xesj.long_process.Permission;
import xesj.app.system.MainApplication;
import xesj.app.util.main.Home;
import xesj.app.util.main.Page;

/**
 * Egyetlen hosszan tartó folyamat lekérdezése
 */
@Controller
public class OneController {
  
  public static final String 
    PATH = "/long-process/one",
    PATH_LOG = "/long-process/one/log",
    PATH_TRACE = "/long-process/one/trace";
  
  private static final String 
    VIEW = "/long_process/one.html",
    VIEW_LOG = "/long_process/one-log.html",
    VIEW_TRACE = "/long_process/one-trace.html";

  @Autowired MainApplication application;  
  @Autowired ServletContext servletContext;
  
  /**
   * Egy folyamat főbb adatainak megjelenítése
   * GET: /long-process/one
   */
  @GetMapping(PATH)
  public String get(@RequestParam Long id, Model model) {

    // Ellenőrzés
    check(id);
    
    // Előkészítés
    LongProcess longProcess = application.getProcesses().get(id);
    
    // Model beállítása
    model.addAttribute("page", new Page("Folyamat kezelése", AllController.PATH));
    model.addAttribute("freshUrl", servletContext.getContextPath() + FreshController.PATH);
    model.addAttribute("id", id);
    model.addAttribute("name", longProcess.getName());
    model.addAttribute("permission", longProcess.getPermission().description);
    model.addAttribute("readPermission", longProcess.getPermission() == Permission.READ);
    
    // View megjelenítése
    return VIEW;
    
  }

  /**
   * Egy folyamat naplójának megjelenítése
   * GET: /long-process/one/log
   */
  @GetMapping(PATH_LOG)
  public String log(@RequestParam Long id, Model model) {
    
    // Ellenőrzés
    check(id);
    
    // Előkészítés
    LongProcess longProcess = application.getProcesses().get(id);

    // Model beállítása    
    model.addAttribute("page", new Page("Folyamat napló megtekintése", Home.CLOSE));
    model.addAttribute("id", id);
    model.addAttribute("name", longProcess.getName());
    model.addAttribute("log", longProcess.getLog());
    
    // View megjelenítése
    return VIEW_LOG;
    
  }
  
  /**
   * Egy folyamat stack trace-ének megjelenítése
   * GET: /long-process/one/trace
   */
  @GetMapping(PATH_TRACE)
  public String trace(@RequestParam Long id, Model model) {
    
    // Ellenőrzés
    check(id);
    
    // Előkészítés
    LongProcess longProcess = application.getProcesses().get(id);

    // Model beállítása    
    model.addAttribute("page", new Page("Folyamat hiba megtekintése", Home.CLOSE));
    model.addAttribute("id", id);
    model.addAttribute("name", longProcess.getName());
    model.addAttribute("trace", longProcess.getTrace());
    
    // View megjelenítése
    return VIEW_TRACE;
    
  }

  /**
   * Folyamat azonosító ellenőrzése
   */
  private void check(Long id) {
    
    // Létezik ilyen azonosító ?
    LongProcess longProcess = application.getProcesses().get(id);
    if (longProcess == null) {
      throw new RuntimeException("Nem létezik ilyen folyamat azonosító: " + id);
    }

    // Jogosultság lekérdezése
    if (longProcess.getPermission() == null) {
      throw new RuntimeException("Nem jogosult a folyamat megtekintésére!");
    }
    
  }
  
  // ====
}
