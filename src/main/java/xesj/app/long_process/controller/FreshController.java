package xesj.app.long_process.controller;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import xesj.app.system.MainApplication;
import xesj.app.util.json_response.JsonResponse;
import xesj.app.util.json_response.JsonResponseError;
import xesj.long_process.LongProcess;
import xesj.long_process.Permission;
import xesj.long_process.Request;
import xesj.long_process.RequestException;
import xesj.tool.ExceptionTool;

/**
 * Egyetlen hosszan tartó folyamat friss állapotának lekérdezése, és esetleg státusz kérés
 */
@Controller
public class FreshController {
  
  public static final String PATH = "/long-process/fresh";
  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("uuuu.MM.dd HH:mm:ss");

  @Autowired MainApplication application;  
  
  /**
   * GET: /long-process/fresh
   * A folyamat legfrissebb adatainak lekérdezése, és esetleg kérés a folyamathoz.
   * @param id Folyamat azonosító.  
   * @param request Kérés a folyamathoz.
   */
  @GetMapping(PATH)
  @ResponseBody
  public JsonResponse fresh(@RequestParam Long id, @RequestParam(required = false) Request request) {
      
    // Előkészítés
    JsonResponse jsonResponse = new JsonResponse();
    String requestException = null;
      
    // Létezik ilyen azonosító ?
    LongProcess longProcess = application.getProcesses().get(id);
    if (longProcess == null) {
      throw new RuntimeException("Nem létezik ilyen folyamat azonosító: " + id);
    }

    // Jogosultság lekérdezése
    Permission permission = longProcess.getPermission();
    if (permission == null) {
      throw new RuntimeException("Nem jogosult a folyamat megtekintésére!");
    }

    // Csak teljes hozzáférés jogosultság esetén indítható kérés
    if (permission != Permission.ALL && request != null) {
      throw new RuntimeException("'" + Permission.READ.description + "' jogosultság esetén nem indítható kérés!");
    }
    
    // Kérés végrehajtása
    if (request != null) {
      try {
        switch (request) {
          case CLEAR    -> longProcess.clearRequest(); 
          case RUN      -> longProcess.runRequest(); 
          case PAUSE    -> longProcess.pauseRequest();
          case CONTINUE -> longProcess.continueRequest();
          case BREAK    -> longProcess.breakRequest();
        }
      }  
      catch (RequestException rse) {
        requestException = rse.getMessage();
      }
    }  
    jsonResponse.putData("requestException", requestException);
    
    // Indult
    String startText = null;
    if (longProcess.getStart() != null) {
      startText = longProcess.getStart().format(DATE_TIME_FORMATTER);
    }
    jsonResponse.putData("start", startText);
    
    // Befejeződött
    String endText = null;
    if (longProcess.getEnd() != null) {
      endText = longProcess.getEnd().format(DATE_TIME_FORMATTER);
    }
    jsonResponse.putData("end", endText);
    
    // Státusz
    jsonResponse.putData("status", longProcess.getStatus().name());
    jsonResponse.putData("statusDescription", longProcess.getStatus().description);
    
    // Kérés
    jsonResponse.putData("request", (longProcess.getRequest() == null ? null : longProcess.getRequest().description));
    
    // Üzenet
    jsonResponse.putData("message", longProcess.getMessage());
    
    // Haladás
    jsonResponse.putData("progress", longProcess.getProgress());
    
    // Válasz 
    return jsonResponse;
    
  }
  
  /**
   * Exception handler
   */
  @ExceptionHandler(Exception.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public JsonResponse exceptionHandler(Exception e) {

    // Válasz
    return new JsonResponse()
      .setErrorType(JsonResponseError.Type.UNEXPECTED)
      .addErrorMessage(e.getMessage())
      .setErrorStack(ExceptionTool.traceString(e));

  }  
  
  // ====
}
