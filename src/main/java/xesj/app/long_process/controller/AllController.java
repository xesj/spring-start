package xesj.app.long_process.controller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import xesj.app.util.main.Home;
import xesj.app.util.main.Page;

/**
 * Az összes hosszan tartó folyamat listázása
 */
@Controller
public class AllController {
  
  public static final String PATH = "/long-process/all";
  private static final String VIEW = "/long_process/all.html";
  
  /**
   * GET: /long-process/all
   */
  @GetMapping(PATH)
  public String get(Model model) {
    
    // Model beállítása
    model.addAttribute("page", new Page("Folyamatok megtekintése", Home.MENU));
    
    // View megjelenítése
    return VIEW;
    
  }
 
  // ====
}
