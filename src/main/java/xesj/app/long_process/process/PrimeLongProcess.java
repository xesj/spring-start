package xesj.app.long_process.process;
import java.util.Arrays;
import xesj.long_process.BreakException;
import xesj.long_process.LongProcess;
import xesj.long_process.Permission;

/**
 * Prímszámok keresése, hosszan tartó folyamat
 */
public class PrimeLongProcess extends LongProcess {
  
  /**
   * Keresési tartomány kezdete. 
   * Megengedett érték: 2 - Long.MAX_VALUE
   */
  private final long fromNumber;

  /**
   * Keresési tartomány vége. 
   * Megengedett érték: 2 - Long.MAX_VALUE
   * Kevesebb, és több is lehet mint a fromNumber.
   */
  private final long toNumber;

  /**
   * Találatok maximális száma. 
   * Megengedett érték: 1 - 65536
   * A keresés véget ér, ha ennyi prímszámot találtunk.
   */
  private final int primeMaximum;
  
  /**
   * Talált prímszámok
   */
  private Long[] primes;
  
  /**
   * Konstruktor
   */
  public PrimeLongProcess(long fromNumber, long toNumber, int maximum) {

    // Ellenőrzés
    if (fromNumber < 2) throw new RuntimeException("A fromNumber minimális értéke 2!");
    if (toNumber < 2) throw new RuntimeException("A toNumber minimális értéke 2!");
    if (maximum < 1 || maximum > 65536) throw new RuntimeException("A maximum csak 1 - 65536 között lehet!");
    
    // Beállítás
    this.fromNumber = fromNumber;
    this.toNumber = toNumber;
    this.primeMaximum = maximum;
    this.primes = new Long[maximum];
    
  }

  /**
   * Folyamat neve, leírása.
   */
  @Override
  public String getName() {
    
    return 
      "Prímszám keresés a(z) " + fromNumber + " - " + toNumber + " tartományban." +
      " Maximum " + primeMaximum + " darab keresése.";

  }
  
  /**
   * Jogosultság lekérdezése. 
   */
  @Override
  public Permission getPermission() {
    
    return Permission.ALL;

  }

  /**
   * A folyamat naplójának lekérdezése
   */
  @Override
  public String getLog() {
    
    StringBuilder log = new StringBuilder();
    log.append("Talált prímszámok:\n");
    int i = 0; 
    for (Long l: primes) {
      if (l != null) {
        log.append(l).append("\n");
        i++;
      }
      else {
        break;
      }
    }
    log.append("Összesen: ").append(i).append(" darab.\n");
    return log.toString();
    
  }
  
  /**
   * Prímszámok keresése
   */
  @Override
  protected void runInner() throws BreakException {

    // Talált prímszámok száma 
    int primeCounter = 0; 
    
    // Prímszám tömb kiürítése
    Arrays.fill(primes, null);
    
    // Ciklus hossza
    long loopMaximum =  Math.abs(toNumber - fromNumber) + 1; 
    
    // Haladási irány (-1, 0, +1)
    long direction = Long.signum(toNumber - fromNumber);

    // Végighaladás a kért intervallumon   
    message = "Keresés";
    long loopCounter = 0;
    for (long n = fromNumber; primeCounter < primeMaximum && loopCounter < loopMaximum; n += direction) {
      loopCounter++;
      
      // Szám ellenőrzése
      boolean prime = true;
      for (long i = 2; i <= Math.sqrt(n); i++) {
        if (n % i == 0) {
          prime = false;
          break;
        }
      }
      if (prime) {
        primeCounter++;
        primes[primeCounter - 1] = n; 
      }
      message = "Ellenőrzött szám: " + loopCounter + " darab. Talált prímszám: " + primeCounter + " darab.";
      // End: szám ellenőrzése
      
      // Kérés figyelése
      listen(); 
    } 
    
    // Záró üzenet
    message = "A prímszámok keresése befejeződött";
    
  }
  
  // ====
}
