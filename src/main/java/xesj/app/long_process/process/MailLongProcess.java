package xesj.app.long_process.process;
import xesj.long_process.BreakException;
import xesj.long_process.LongProcess;
import xesj.long_process.Permission;
import xesj.tool.ThreadTool;

/**
 * Levélküldés szimuláció, hosszan tartó folyamat.
 * A counter változó néhány speciális beállítása esetén különlegesen viselkedik:
 * 
 *   - counter = 101 esetén nincs jogosultság
 *   - counter = 102 esetén READ jogosultság van
 *   - counter = 103 esetén ERROR státusszal fejeződik be: "Megszakadt a kapcsolat az email szerverrel"
 */
public class MailLongProcess extends LongProcess {
  
  /**
   * Kiküldendő levelek száma. Minimum: 1, maximum 10000
   */
  private final long counter;

  /**
   * Egy levél kiküldésének az ideje millisecundum-ban. Minimum: 1
   */
  private final long sendTime;
  
  /**
   * Log
   */
  private StringBuilder log;
  
  /**
   * Konstruktor
   */
  public MailLongProcess(long counter, long sendTime) {
    
    // Ellenőrzés
    if (counter < 1 || counter > 10000) throw new RuntimeException("A counter csak 1-10000 lehet!");
    if (sendTime < 1) throw new RuntimeException("A sendTime minimális értéke 1!");
    
    // Beállítás
    this.counter = counter;
    this.sendTime = sendTime;
    this.log = new StringBuilder();

  }
  
  /**
   * A folyamat megnevezése.
   * - counter = 103 esetén más szöveg
   */
  @Override
  public String getName() {
    
    if (counter != 103) {
      return "Levélküldés szimuláció. Levelek száma: " + counter + ". Egy levél küldésének ideje: " + sendTime + " ms.";
    }
    else {
      return "Levélküldés szimuláció, mely ERROR státusszal ér véget.";
    }
    
  }

  /**
   * Jogosultság lekérdezése. 
   * - counter = 101 esetén nincs jog
   * - counter = 102 esetén READ jog van
   * - egyéb esetben ALL jog van
   */
  @Override
  public Permission getPermission() {

    if (counter == 101) {
      return null;
    }
    if (counter == 102) {
      return Permission.READ;
    }
    return Permission.ALL;

  }

  /**
   * A folyamat naplójának lekérdezése
   */
  @Override
  public String getLog() {
    
    return log.toString();
    
  }
  
  /**
   * Levélküldés szimuláció
   */
  @Override
  protected void runInner() throws BreakException {
    
    // Előkészítés
    message = "A levélküldés elkezdődött";
    progress = 0;
    log.setLength(0);
    log.append("Elküldött levelek:\n");
    
    // Küldés
    for (long i = 1; i <= counter; i++) {
      ThreadTool.sleep(sendTime); // Levélküldés várakozásként szimulálva
      message = i + " levél elküldve";
      log.append("joe").append(i).append("@gmail.com\n");
      progress = (int)(100.0 * i / counter);
      if (counter == 103 && i == 50) {
        throw new RuntimeException("Megszakadt a kapcsolat az email szerverrel!");
      }
      listen(); // Kérés figyelése
    }
    
    // Záró üzenet
    message = "A levélküldés befejeződött";
    
  }
  
  // ====
}
