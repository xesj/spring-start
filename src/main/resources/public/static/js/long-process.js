/******************************************************************************************************************************* 
 * 
 * Modul: 
 * Egyetlen hosszan tartó folyamat kezelése.
 * 
 * Funkciók:
 * - Kapcsolattartás a FreshController java osztállyal
 * - Folyamat állapotának megjelenítése a weboldalon
 * - Kérés küldése a folyamatnak
 * - Hibás kérés megjelenítése a javascript konzolon
 * - A FreshController-ben keletkező java stack trace megjelenítése a weboldalon
 * 
 ******************************************************************************************************************************/

// Import
import {httpServiceCall} from './xesjslib/service.js';

// Előkészítés
let timerId = null;

// Általános elemek
const id = document.getElementById('id').innerHTML;
const freshUrl = document.getElementById('freshUrl').innerHTML;
const freshIntervalElem = document.getElementById('freshInterval');
const startElem = document.getElementById('start');
const endElem = document.getElementById('end');
const messageElem = document.getElementById('message');
const traceElem = document.getElementById('trace');
const freshExceptionElem = document.getElementById('freshException');

// Status elemek
const statusIconContainerElem = document.getElementById('statusIconContainer');
const statusElem = document.getElementById('status');

// Request elemek
const requestContainerElem = document.getElementById('requestContainer');
const requestElem = document.getElementById('request');

// Progress elemek
const progressContainerElem = document.getElementById('progressContainer');
const progressBarElem = document.getElementById('progressBar');
const progressTextElem = document.getElementById('progressText');

// Ikonok
const playIconElem = document.getElementById('playIcon');
const pauseIconElem = document.getElementById('pauseIcon');
const stopIconElem = document.getElementById('stopIcon');

// Click listener ráhelyezés azokra a html elemekre, melyek rendelkeznek "data-request" attribútummal
let elements = document.querySelectorAll('[data-request]');
for (let elem of elements) {
  elem.addEventListener('click', request);
}

// Frissítés intervallum elemre listener helyezés
freshIntervalElem.addEventListener('change', function () {
  if (timerId !== null) {
    clearInterval(timerId);
    timerId = null;
  }  
  if (freshIntervalElem.value !== '') {
    fresh(null);
    timerId = setInterval(fresh, 1000 * freshIntervalElem.value, null);
  }  
});

// Frissítés intervallum elemen change-event végrehajtás
freshIntervalElem.dispatchEvent(new Event('change'));

/**
 * Kérés a folyamathoz
 */
function request(event) {
  
  let elem = event.currentTarget;
  let requestCode = elem.getAttribute('data-request');
  fresh(requestCode);
  
}

/**
 * Backend fresh hívása 
 */
async function fresh(requestCode) {
  
  // Url összeállítása
  let url = freshUrl + '?id=' + encodeURIComponent(id);
  if (requestCode !== null) {
    url += '&request=' + encodeURIComponent(requestCode); 
  }
  
  // Backend hívás
  let result = await httpServiceCall(url);
  
  // 200-as http-válasz
  if (result.status === 200) {
    
    // Előkészítés
    let data = result.json.data;
    let display = null;
    
    // Indult
    startElem.textContent = data.start;
    
    // Befejeződött
    endElem.textContent = data.end;
    
    // Státusz
    let statusColor = null;
    if (['NEW'].includes(data.status)) statusColor = 'text-secondary';
    if (['RUN','PAUSE','SUCCESS'].includes(data.status)) statusColor = 'text-success';
    if (['ERROR','BREAK'].includes(data.status)) statusColor = 'text-danger';
    statusIconContainerElem.className = statusColor;
    playIconElem.style.display = (['RUN'].includes(data.status) ? 'block' : 'none');
    pauseIconElem.style.display = (['PAUSE'].includes(data.status) ? 'block' : 'none');
    stopIconElem.style.display = (['RUN','PAUSE'].includes(data.status) ? 'none' : 'block');
    statusElem.className = statusColor;
    statusElem.textContent = data.statusDescription; 
    
    // Üzenet
    messageElem.textContent = data.message; 
    
    // Haladás
    progressContainerElem.style.display = (data.progress === null ? 'none' : 'block');
    progressBarElem.style.width = data.progress + '%'; 
    progressTextElem.textContent = data.progress + '%'; 
    
    // Hiba
    traceElem.style.display = (data.status === 'ERROR' ? 'block' : 'none');

    // Kérés
    if (data.request === null) {
      requestContainerElem.style.display = 'none';
    }
    else {
      requestContainerElem.style.display = 'block';
      requestElem.textContent = data.request + '...'; 
    }
    
    // requestException log-olás konzolra
    if (data.requestException !== null) {
      console.log(data.requestException);
    }
  
  }
  // Nem 200-as http-válasz. Csak akkor írjuk ki, ha még nem volt kiírás.
  else {
    
    if (freshExceptionElem.innerHTML.trim() === '') {
      freshExceptionElem.textContent = 'Sikertelen oldal frissítés!\n\n' + result.json.error.stack;
    }  
    
  }

}

/******************************************************************************************************************************/