/*******************************************************************************************************************************
 *
 * Modul: javascript hiba megjelenítő. 
 * A modul célja, hogy a weboldalon keletkező javascript hibák ne maradjanak rejtve. 
 * A felhasználó általában nem nézi a konzolt, így lehet, hogy a javascript hibákat sem észleli, és a fejlesztőkhöz 
 * sem jut el a hibajelzés. Ez a modul viszont a html oldalon jelzi, hogy hány javascript hiba keletkezett, 
 * felhívva arra a felhasználó figyelmét. A hibát megjelenítő html elemet (error display -t) a weboldalnak kell tartalmaznia, 
 * az elemet nem ez a modul készíti el. A modul csak láthatóvá teszi ezt a html elemet a "display" css-jellemzőjének 
 * módosításával. Egy weboldalon több ilyen html elem is lehet. Egy elemen belül 0 vagy 1 hibaszámláló lehet. 
 * 
 * Példa egy "error display" html elemre:
 * 
 *     <span id="error-display" 
 *           data-xesjslib-error-display="block" 
 *           title="A konzolban (F12) látható hibát jelezze a fejlesztőknek!">
 *       javascript error: <span data-xesjslib-error-display-counter/>
 *     </span>  
 *   
 *   A fenti html-elemhez tartozó css, hogy fixen a jobb alsó sarokban jelenjen meg pirosan:
 *   
 *     #error-display {
 *       display: none; 
 *       position: fixed;
 *       bottom: 0;
 *       right: 0;
 *       background-color: red; 
 *       color: white; 
 *       padding: 3px 5px; 
 *       border-radius: 3px;
 *       font-size: 12px;
 *     }  
 * 
 * A "data-xesjslib-error-display" jelzi, hogy ez egy javascript error megjelenítő html elem, az értéke pedig amire 
 * a "display" css-jellemzőt be fogja állítani a javascript, ha error keletkezik. A html-elemnek alapból "display: none"
 * jellemzővel kell beállítva lennie, hogy ne látszódjon. A példában van hibaszámláló is, ezt a 
 * "data-xesjslib-error-display-counter" jelzi. Ha létezik ilyen, akkor a javascript ennek a belsejébe fogja elhelyezni 
 * a hibák számát.
 * 
 * Ezt a javascript modult a többi javascript elé kell beilleszteni, mert csak az utána futó javascript hibákat 
 * tudja megjeleníteni. Javasolt a bootstrap javascript után, de a többi javascript elé helyezni.  
 * Beillesztés:
 * 
 *   <script type="module" src=".../error-display.js"></script>
 * 
 * A modul a lefutásakor automatikusan felderíti a "data-xesjslib-error-display" attribútummal ellátott html elemeket, 
 * és elvégzi a szükséges teendőket. 
 * Megjegyzés: Ha a weboldal nemlétező javascript fájlra hivatkozik, azt nem tudja megjeleníteni, mivel az nem javascript hiba.
 * 
 ******************************************************************************************************************************/

// Hiba számláló
let counter = 0;

// "data-xesjslib-error-display" attribútummal rendelkező elemek keresése
let elements = document.querySelectorAll('[data-xesjslib-error-display]');

/**
 * Error handler függvény
 */ 
function errorHandler() {
  counter++;
  for (let elem of elements) {
    // display módosítás
    let display = elem.getAttribute('data-xesjslib-error-display');
    elem.style.display = display;
    // számláló módosítása, ha létezik
    let counterElem = elem.querySelector('[data-xesjslib-error-display-counter]');
    if (counterElem !== null) {
      counterElem.innerHTML = counter;      
    }
  }
}

// Error handler beállítása normál esetre
window.onerror = errorHandler;

// Error handler beállítása async függvényekre
window.onunhandledrejection = errorHandler;

/******************************************************************************************************************************/