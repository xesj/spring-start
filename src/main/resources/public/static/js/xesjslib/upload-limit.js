/*******************************************************************************************************************************
 * 
 * Modul: Fájl feltöltés limitálása, méret, kiterjesztés, darabszám szerint. 
 * A weblapon több form, és több fájlfeltöltő input mező is lehet.
 * Azokat az input mezőket keresi meg, melyek form-on belül vannak, 
 * és rendelkeznek a "data-xesjslib-upload-limit-size" attribútummal.
 * Az input mező, melyhez a speciális attribútumokat hozzá lehet rendelni:
 * 
 *   <input type="file" .../>
 *   
 * Az input mezőket a következő attribútumokkal lehet ellátni:  
 * 
 *   - data-xesjslib-upload-limit-size="..."
 *     Korlátozza a feltöltendő fájl méretét. Tisztán szám (bájtban megadva a méret), vagy szám + utótag.
 *     A használható utótagok: "K", "M", "G", "T", "P" (kilo, mega, giga, tera, peta), melyek az 1024 hatványai.
 *     Az attribútum megadása kötelező. Nem lehet null, és negatív érték.
 *     Példák: "4700", "1.5K", "4M", "3.75G"
 *       
 *   - data-xesjslib-upload-limit-extension="..."
 *     Korlátozza a feltöltendő fájl kiterjesztését. Vesszővel kell az értékeket elválasztani, számít a kis/nagybetű. 
 *     Tartalmazhat space-t is, melyek trim-melve lesznek. Nem kötelező megadni.
 *     Példák: 
 *       Ez a 3 féle kiterjesztés: "jpg, jpeg,PNG"
 *       Csak pdf: "pdf"
 *       Pdf, és kiterjesztés nélküli fájlok: "pdf,"
 *       Csak kiterjesztés nélküli fájlok (itt kell a space): " "
 *     
 *   - data-xesjslib-upload-limit-multiple="..."
 *     Korlátozza a feltöltendő fájlok darabszámát. Akkor érdemes használni, ha az input mező rendelkezik
 *     "multiple" attribútummal. Egész szám, mely nem lehet negatív. Nem kötelező megadni.
 *     Példák: "3"
 *     
 * Egy teljes példa:
 * 
 *   <input type="file" multiple 
 *          data-xesjslib-upload-limit-size="23.5K" 
 *          data-xesjslib-upload-limit-extension="png, jpg"
 *          data-xesjslib-upload-limit-multiple="3"
 *          />
 *     
 * A modul használata: a html-oldalon hivatkozni kell erre a modulra: 
 * 
 *   <script type="module" src=".../upload-limit.js"></script>
 *
 * A modul futásakor automatikusan felderíti a "data-xesjslib-upload-limit-size" attribútummal ellátott html elemeket, 
 * és elvégzi a szükséges teendőket. Megkeresi a formot (formokat), és egy esemény kezelőt telepít ami a submit előtt lefut.
 * Submit előtt végignézi azokat a fájl feltöltő mezőket, melyekre limit van beállítva, és a limiteket ellenőrzi.
 * 
 * Ha limit-hibát talál, akkor egy bootstrap modal ablakban kiírja a hibát, és nem engedi a formot submit-álni.
 * A bootstrap modal ablakot a html oldalnak kell tartalmaznia úgy, hogy a html-id értéke "xesjslib-upload-limit-alert-id" 
 * legyen. A limit-hibaüzenetet a modal ablak "modal-body" osztálynévvel ellátott html elem belsejébe helyezi el.
 * Ha az oldalon nincs ilyen modal ablak, az nem probléma, ilyenkor a limit-hibaüzenet a javascript alert() 
 * függvénnyel lesz kiírva.
 * 
 ******************************************************************************************************************************/

// Import
import {stringToInteger, stringToNumberKMG} from './convert.js';
import {XesjDataError, XesjLimitError} from './error.js';

// Formok keresése
let formElements = document.querySelectorAll('form');

// Ciklus a form elemeken 
for (let formElem of formElements) {
  
  // Létezik a formon belül "data-xesjslib-upload-limit-size" attribútummal ellátott elem ?
  let limitElem = formElem.querySelector('[data-xesjslib-upload-limit-size]');
  
  // Létező limit-elem esetén eseménykezelő helyezése a form-ra
  if (limitElem !== null) {
    formElem.addEventListener('submit', submitHandler);
  }
  
}

/**
 * Form submit handler.
 */ 
function submitHandler(event) {

  try {

    // Form elem
    let formElem = event.currentTarget;

    // Formon belüli "data-xesjslib-upload-limit-size" attribútummal ellátott elemek megkeresése
    let limitElements = formElem.querySelectorAll('[data-xesjslib-upload-limit-size]');
    for (let limitElem of limitElements) {
      limitElemHandler(limitElem);    
    }
    
  }
  catch (error) {
    event.preventDefault();
    if (error instanceof XesjLimitError) {
      limitAlert(error.message);
    }
    else {
      throw error;
    }
  }

}

/**
 * Limit-tel ellátott html-elem kezelő.
 * @param {object} elem Html-elem, melyre limit-ek vannak beállítva. 
 */ 
function limitElemHandler(elem) {
  
  // Size limit meghatározása egész számként
  let sizeLimitText = elem.getAttribute('data-xesjslib-upload-limit-size');
  let sizeLimit = stringToNumberKMG(sizeLimitText);
  if (sizeLimit !== null) sizeLimit = Math.round(sizeLimit);
  if (sizeLimit === null || sizeLimit < 0) {
    throw new XesjDataError(`Upload size limit nem lehet null, vagy negatív szám: ${sizeLimitText}`);
  }
  
  // Extension limit meghatározása: null, vagy legalább 1 elemű tömb
  let extensionLimitText = elem.getAttribute('data-xesjslib-upload-limit-extension');
  let extensionLimitArray = null;
  if (extensionLimitText !== null && extensionLimitText !== '') {
    extensionLimitArray = extensionLimitText.split(',');
  }  

  // Multiple limit meghatározása: null, vagy egész szám
  let multipleLimitText = elem.getAttribute('data-xesjslib-upload-limit-multiple');
  let multipleLimit = stringToInteger(multipleLimitText);
  if (multipleLimit < 0) {
    throw new XesjDataError(`Upload multiple limit nem lehet negatív szám: ${multipleLimitText}`);
  }
  
  // Ellenőrzések
  for (let file of elem.files) {
    
    // Méret ellenőrzés
    if (file.size > sizeLimit) {
      throw new XesjLimitError(`A(z) ${file.name} fájl feltöltése nem lehetséges, mert túl nagy méretű!`);
    }
    
    // Kiterjesztés ellenőrzése
    if (extensionLimitArray !== null) {
      let good = false;
      let pos = file.name.lastIndexOf('.');
      let ext1 = (pos === -1 ? '' : file.name.substring(pos + 1));
      for (let ext2 of extensionLimitArray) {
        if (ext1 === ext2.trim()) {
          good = true;
          break;
        }
      }
      if (!good) {
        throw new XesjLimitError(`A(z) ${file.name} fájl feltöltése nem lehetséges, mert a kiterjesztése nem megengedett!`);
      }
    }
    
  } // End: for  
  
  // Darabszám ellenőrzése
  if (multipleLimit !== null && elem.files.length > multipleLimit) {
    throw new XesjLimitError(
      `A(z) ${elem.files.length} darab fájl feltöltése nem lehetséges, mert meghaladja a megengedett darabszámot!`
    );
  }
  
}

/**
 * Limit túllépés hibaüzenet megjelenítése.
 * Ha a weblap tartalmaz "xesjslib-upload-limit-alert-id" html-id-vel elemet (modal ablakot), akkor abban jeleníti meg a hibát,
 * ha nincs ilyen elem, akkor sima alert() függvénnyel írja ki.
 * @param {string} message Hibaüzenet 
 */
function limitAlert(message) {
  
  // Modal megkeresése
  let modalElem = document.getElementById('xesjslib-upload-limit-alert-id');
  
  // Ha nem létezik a modal, akkor sima alert()-tel írjuk ki a hibát.
  if (modalElem === null) {
    alert(message);
    return;
  }
  
  // Modal body elemének megkeresése, és tartalom írása
  let modalBodyElem = modalElem.querySelector('.modal-body');
  modalBodyElem.innerHTML = message;

  // Modal megjelenítése
  let modalWindow = new bootstrap.Modal(modalElem);
  modalWindow.show();
  
}

/******************************************************************************************************************************/