+--------+
| README |
--------------------------------------------------------------------------------------------------------------------------------
HAZNÁLAT

- A xesjslib (xesj javascript library) a Bootstrap css-keretrendszerre épül, ezért a fő javascript moduljában (main.js) 
  található egy BOOTSTRAP_COMPATIBLE konstans, mely jelzi, hogy melyik Bootstrap verzióval tud együttműködni. 
  A xesjslib-nek azért van szüksége egy css-keretrendszerre, mert az általa megjelenített html elemeknek egy adott 
  kinézethez kell igazodniuk.

- A xesjslib ZIP-formátumban van publikálva a https://xesj.hu weblapon.
  A zip-fájlt ki kell csomagolni. Egy tetszőleges projektben a javascriptek közé kell elhelyezni úgy,
  hogy lehetőleg egy külön könyvtárba kerüljön, például: js/xesjslib. A xesjslib összes fájlját bele kell rakni a 
  projektbe akkor is, ha nem minden eleme lesz használva. A fájloknak ugyanabban a könyvtárban kell lenniük.
  A xesjslib modulokból áll, a modulok egymásra hivatkoznak, és nem találnák meg egymást ha külön-külön könyvtárban 
  lennének. A "doc" könyvtár, és tartalma nem szükséges a helyes működéshez, de ajánlatos azt is betenni a projektbe.
  A projekt html-oldalaiba csak azokat a xesjslib modulokat kell berakni, melyeket az adott oldal használni szeretne.
  Egy modul használatának leírása megtalálható a modul kommentjében. A xesjslib tartalmaz egy main.css fájlt, 
  ezt minden html-oldalba rakjuk be, mivel a xesjslib javascriptjei hivatkoznak olyan css-osztályokra, melyek 
  a main.css-ben vannak definiálva. 

--------------------------------------------------------------------------------------------------------------------------------
WEB BROWSER SUPPORT

- Ezekkel a web böngészőkkel lett tesztelve, ezeknek az újabb verzióival tud együttműködni:

  - Firefox  
  - Chrome
  - Edge

- Más böngészőkkel is jól működhet, de ezekkel nincs tesztelve. 

--------------------------------------------------------------------------------------------------------------------------------
NAMESPACE

- A xesjslib olyan azonosítókat alkalmaz, melyek nagyon nagy valószínűséggel 
  nem ütköznek egy ismeretlen html oldal azonosítóival.
  Ezért ezeket az azonosítókat használja:

    - Html id            -->    "xesjslib-..." 
    - Css osztály        -->    "xesjslib-..."
    - Data attribútum    -->    "data-xesjslib-..."

--------------------------------------------------------------------------------------------------------------------------------